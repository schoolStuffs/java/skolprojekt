package it.lundstedt.erik;


import it.lundstedt.erik.menu.Menu;

import java.util.Scanner;


public class Main {

public static void runMenu()
{
	String[] s = new String[]{};
	Main.main(s);
}
	public static void main(String[] args)
	{
		
		String[] mainMenuHeadder={
				"WELCOME",
				"PROGRAM MENU"
		};
		
		String[] mainMenuItems=
				{
				"press 0 Info/Help",
				"press 1 arrays.",
				"press 2 bank.",
				"press 3 calculator.",
				"press 4 game.",
				"press 5 generics",
				"press 6 to exit"
				};
		Menu mainMenu=new Menu();
		mainMenu.setHeader(mainMenuHeadder);
		mainMenu.setMenuItems(mainMenuItems);
		mainMenu.drawMenu();
		Program.input(Menu.getChoiceStr());
	}
}
