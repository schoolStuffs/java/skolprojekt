package it.lundstedt.erik;

/*
*this modified code are under the Apache License
*Version 2.0, January 2004
*http://www.apache.org/licenses/
*/

import it.lundstedt.erik.maths.Calc;
import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.minigame.Play;

public class Program {
	private static String temp; // String for storing temporary information.

	public static void input(String input) {
		// Check if input is valid
		String[] args = input.split(" ");
		if (args.length >= 1) {
			int inputInt = Integer.parseInt(args[0]);
			switch (inputInt) {
			case 0:
				help();
				break;
			case 1:
				Arrays.sortArrays();
				break;
			case 2:
				Bank.bank();
				break;
			case 3:
				Calc.calculator();
				break;
			case 4:
				Play.play();
				break;
			case 5:
			new Generic<>("hello world");
			Main.runMenu();
			
			break;
				case 6:
				exit();
			break;
				default:
			Menu.error("Input is to high or to low.");
			}
		} else {
			Menu.error("Input is invalid.");
		}

	}

	// An example feature.
	private static void feature() {
		String text1 = Menu.getInput("Enter text: ");

		char[] text2 = text1.toCharArray();

		System.out.print("Reversed text: ");
		for (int i = text2.length - 1; i >= 0; i--) {
			System.out.print(text2[i]);
		}

		System.out.println();
	}

	// Display help information.
	private static void help() {
		System.out.println("Command 0: Display help.\n"
			+ "Command 1: arrays.\n"
			+ "Command 2: your bank.\n"
			+ "Command 3: Exit.");
	}

	// Prompt user to exit the program.
	public static void exit()
	{
		temp = Menu.getInput("Are you sure you want to exit? (y/n) ");
		if (temp.equals("y") || temp.equals("yes")) {
			// We do print instead of println to avoid a ghost line at the bottom.
			System.out.print(Strings.ok + "Exiting...");
			System.exit(0);
		} else {
			return;
		}
	}

	
	
static void startArrays()
{
	Arrays.sortArrays();
}
static void startBank()
{
	Bank.bank();
}
static void startCalc()
{
	Calc.calculator();
}




}
