package it.lundstedt.erik.minigame;


import it.lundstedt.erik.menu.Menu;

import java.util.*;

public class Play
{
	public static void play()
	{
		String[] menuHeadder={
				"WELCOME",
				"game menu"
		};
		
		String[] gameMenuItems=
				{
				"0",
				"1",
				"2"
				};
		Menu gameMenu=new Menu();
		Player player=new Player();
		
		
		gameMenu.setHeader(menuHeadder);
		gameMenu.setMenuItems(gameMenuItems);
		gameMenu.drawMenu();
		
		
		Skelengel skelengel=new Skelengel();
		skelengel.attack();
		
	}

}
