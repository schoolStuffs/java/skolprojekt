package it.lundstedt.erik;

import it.lundstedt.erik.menu.Menu;

public class Generic <T>
{
	
	private T t;
	public Generic(T t) {
		this.t = t;
		System.out.println(t);
		String[] head={t.toString()};
		Menu.drawHeader(head);
	}
}
