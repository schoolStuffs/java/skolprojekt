package it.lundstedt.erik.menu;

import java.util.Scanner;

public class Menu
{
	/*
	* keep
	* licence
	*/
	/*example formatting*/
	String[] header={"WELCOME TO OpenMenu","version 5.0","its free and","open source","under the don't be a jerk licence"};
	String[] menuItems={"first","second","third"};
	/*end example formatting*/
	public String[] getHeader()
	{
		return header;
	}
	public void setHeader(String[] header)
	{
		this.header = header;
	}
	public String[] getMenuItems() {
		return menuItems;
	}
	public void setMenuItems(String[] menuItems)
	{
		this.menuItems = menuItems;
	}
	
	public void drawMenu()
	{
		Draw.headder(header);
		for (String menuItem : menuItems)
		{
			Draw.content(menuItem);
		}
		Draw.content(Draw.getPadding());
	}
	public static void drawMenu(String[]header,String[]menuItems)
	{
		Draw.headder(header);
		for (String menuItem : menuItems) {
			Draw.content(menuItem);
		}
		Draw.content(Draw.getPadding());
	}
	public static void drawItems(String[]menuItems)
	{
		for (String menuItem : menuItems) {
			Draw.content(menuItem);
		}
	}
	public static void drawItem(String menuItem)
	{
			Draw.content(menuItem);
	}
	public static void drawHeader(String[]header)
	{
		Draw.headder(header);
	}
	
	
	
	public static void debug(String content,boolean debug)
	{
		if (debug) {
			System.out.print("\u001B[31m");
			Draw.content(Draw.padding);
			Draw.content(content);
			Draw.content(Draw.padding);
			System.out.print("\u001B[0m");
			
		}
	}
	
	public static void error(String content)
	{
			System.out.print("\u001B[31m");
			Draw.content(Draw.padding);
			Draw.content(content);
			Draw.content(Draw.padding);
			System.out.print("\u001B[0m");
	}
	
	public static int getChoice()
	{
		Scanner input = new Scanner(System.in);
		int MenyVal = input.nextInt();
		return MenyVal;
	}
	
	
	public static String getChoiceStr()
	{
		Scanner input = new Scanner(System.in);
		String MenyVal = input.nextLine();
		return MenyVal;
	}
	// Display a message and get the users input.
	public static String getInput(String message) {
		
		Menu.drawItem(message);
		Scanner in = new Scanner(System.in);
		String input = in.next();
		return input;
	}
	
	
}
