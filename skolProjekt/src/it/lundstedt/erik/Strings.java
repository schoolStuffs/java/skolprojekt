package it.lundstedt.erik;




/*
 *this modified code are under the Apache License
 *Version 2.0, January 2004
 *http://www.apache.org/licenses/
 */

public class Strings {
	public static String prompt = ">";
	public static String error =  "[ ERROR ] - ";
	public static String alert =  "[ ALERT ] - ";
	public static String warn =   "[ WARN ] - ";
	public static String ok =     "[ OK ] - ";
}
