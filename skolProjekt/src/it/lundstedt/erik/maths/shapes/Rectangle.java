package it.lundstedt.erik.maths.shapes;

import it.lundstedt.erik.maths.Multiply;
import it.lundstedt.erik.menu.Menu;

public class Rectangle
{
	private static int length;
	//private static String shape;
	private static int side;
	
	public static double menu(String shape) {
		boolean doDebug=true;
		/*example formatting*/
		String[] header = {"RECTANGLE FUNCTION"};
		String[] menuItems = {"Area"};
		
		/*end example formating*/
		
		Menu mainMenu = new Menu();
		mainMenu.setHeader(header);
		mainMenu.setMenuItems(menuItems);
		mainMenu.drawMenu();
		Menu.drawItem("what is the length of your " + shape);
		length = Menu.getChoice();
		if (shape == "rectangle")
		{
			Menu.drawItem("what is the side of your " + shape);
			side = Menu.getChoice();
		}else if (shape.equals("square"))
		{
			side=length;
		}
		Menu.debug(length+" "+side+" "+side*length,doDebug);
		return (Multiply.times(side,length));
	}
}

