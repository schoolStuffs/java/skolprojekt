package it.lundstedt.erik.maths.shapes;

import it.lundstedt.erik.maths.Multiply;
import it.lundstedt.erik.menu.Menu;

public class Circle {
	int radius;
	static double pi =  Math.PI;
	public static double menu()
	{
		
		/*example formatting*/
		String[] header={"CIRCLE FUNCTIONS"};
		String[] menuItems={"0)Area","1)Permimeter"};
		
		/*end example formating*/
		
		Menu mainMenu=new Menu();
		mainMenu.setHeader(header);
		mainMenu.setMenuItems(menuItems);
		mainMenu.drawMenu();
		
		return rep();
	}
	public static double rep()
	{
		int R;
		switch (Menu.getChoice())
		{
			case 0:
				Menu.drawItem("what is the radius of your circle");
				R = Menu.getChoice();
				double newR=R*R;
				return (Multiply.times(pi,newR));
			case 1:

				//2 × pi × radius
				Menu.drawItem("what is the radius of your circle");
				R=Menu.getChoice();
				return 2 * Multiply.times(Math.PI, R*2);
			default:
				System.out.println("ERROR");
				return 0;

		}
		
	}
}
