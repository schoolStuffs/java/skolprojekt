package it.lundstedt.erik.maths;

import it.lundstedt.erik.menu.Menu;

public class Fight //divide
{
	/*
	* stuff to handle "bråktal"(x/y)
	* */
	public static int divide(int x,int y)
	{
		try {
			return x / y;
		}catch (ArithmeticException e)
		{
			String[] header={"ERRORED: "+e,"fixing by adding 2 to the division"};
			Menu.drawHeader(header);
			if (x==0)
			{
				x++;
				x++;
			}
			if (y==0)
			{
				y++;
				y++;
			}
			
			return x / y;
		}
	}
	public static int rest(int x,int y)
	{
		try {
			return x % y;
		}catch (ArithmeticException e)
		{
			String[] header={"ERRORED: "+e,"fixing by adding 2 to the modulus"};
			Menu.drawHeader(header);
			if (x==0)
			{
				x++;
				x++;
			}
			if (y==0)
			{
				y++;
				y++;
			}
			
			return x % y;
		}
	}
}