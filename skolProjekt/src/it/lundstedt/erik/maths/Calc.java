package it.lundstedt.erik.maths;
import it.lundstedt.erik.Program;
import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.maths.shapes.*;
import java.util.Scanner;

/**
 * all of the code in this class are found under the dont be a jerk licence
 *2018-01-11
 */

public class Calc
{
	public static void calculator()
	{
		
		// skapar variabeln input med värdet new Scanner System.in
		Scanner input = new Scanner(System.in);
		// skapa basvariabler här
		
		// skapa program-variabler här
		
		//skriver menyn på skärmen
		String[] menuHeadder={
				"WELCOME",
				"CALCULATOR MENU"
		};
		
		String[] calcMenuItems=
				{
						"press 0 for +",
						"press 1 for -",
						"press 2 for *",
						"press 3 for /",
						"press 4 for circles",
						"press 5 for squares",
						"press 6 for rectangles",
						"press 7 to exit the program"
			/*
						"press 8 for triangles",
						"press 9 for",
						"press 10 for",
						"press 11 for",
						"press 12 for",
						"press 13 for",
						"press 14 for",
						"press 15 for"
			*/
				};
		Menu calcMenu=new Menu();
		calcMenu.setHeader(menuHeadder);
		calcMenu.setMenuItems(calcMenuItems);
		calcMenu.drawMenu();
		//komentera ut EN av kommande två
		
		//int MenyVal=(); 1 2 3 eller 4
		int MenyVal = input.nextInt();
		
		int num0,num1;
		
		switch (MenyVal)
		{
			case 0:
			{
				num0=getnum0();
				num1=getnum1();

				//räknar ut summan
				//skriver ut summan
				Menu.drawItem(" Dina adderade tal: " + num0 +" och "+ num1 + " har summan " + Add.plus(num0,num1) );
				break;
			}
			case 1:
			{
				num0=getnum0();
				num1=getnum1();
				//räknar ut summan
				//skriver ut summan
				Menu.drawItem(" Dina differerade tal: " + num0 +" och "+ num1 + " har differensen " + TakeAway.remove(num0,num1) );
				break;
			}
			case 2:
			{
				num0=getnum0();
				num1=getnum1();
				//räknar ut produkten
				//skriver ut produkten
				Menu.drawItem(" Dina multiplicerade tal: " + num0 +" och "+ num1 + " har produkten " + Multiply.times(num0,num1) );
				break;
			}
			case 3:
			{
				num0=getnum0();
				num1=getnum1();
				//skriver ut differensen
				Menu.drawItem(" Dina dividerade tal: " + num0 +" och "+ num1 + " har kvoten " + Fight.divide(num0,num1) );
				break;
			}
			case 4:
			{
				Menu.drawItem(String.valueOf(Circle.menu()));
				break;
			}
			case 5:
			{
				Menu.drawItem(String.valueOf(Rectangle.menu("square")));
				break;
			}
			case 6:
			{
				Menu.drawItem(String.valueOf(Rectangle.menu("rectangle")));
				break;
			}
			case 7:
			{
				Program.exit();
				break;
			}
		}
		
		
		



	}



	public static int getnum0()
	{
		Scanner input = new Scanner(System.in);
		//frågar efter ett heltal
		Menu.drawItem("type a non decimal number and press enter");
		//får input nr 1
		return input.nextInt();

	}
	public static int getnum1() {
		Scanner input = new Scanner(System.in);
		//frågar efter nästa heltal
		Menu.drawItem("type a second non decimal number and press enter");
		//får input nr 2
		return input.nextInt();
	}
}
